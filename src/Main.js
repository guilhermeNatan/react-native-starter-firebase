import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools';
import {
    View,
} from 'react-native';

import rootReducer from './reducers';
import Router from './navigation/Router';
import DrawerNavigator from './navigation/DrawerNavigator'


// applyMiddleware : usado para combinar varios middlewares
const store = createStore(rootReducer, composeWithDevTools(
    applyMiddleware(reduxThunk),
));


const Main = () => (
    <View style={{ width: '100%', height: '100%' }}>
        <Provider store={store}>
            {/*<DrawerNavigator />*/}
            <Router/>
        </Provider>
    </View>
);

export default Main;
