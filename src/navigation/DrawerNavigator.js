import React from 'react';
import {createAppContainer, createDrawerNavigator} from 'react-navigation';
import {Dimensions} from 'react-native';

import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import MenuDrawer from "../components/MenuDrawer";

// TODO: usar a classe DeviceFeatures para obter a largura
const WIDTH = Dimensions.get('window').width;
const DrawerConfig = {
    drawerWidth: WIDTH * 0.83,
    contentComponent: ({navigation}) => {
        return (<MenuDrawer navigation={navigation}/>)
    }
}


const DrawerNavigador = createDrawerNavigator({
    Home: {
        screen: HomeScreen
    },
    Links: {
        screen: LinksScreen
    },
    Settings: {
        screen: SettingsScreen
    }
}, DrawerConfig);

export default createAppContainer(DrawerNavigador);