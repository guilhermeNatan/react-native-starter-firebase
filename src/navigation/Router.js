import React from 'react';
import {createAppContainer, createStackNavigator} from 'react-navigation';
import {fromLeft} from 'react-navigation-transitions';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import DrawerNavigator from './DrawerNavigator'


const stackNavigator = createStackNavigator({
        Login: {
            screen: LoginScreen,
            navigationOptions: {
                header: null,

            },
        },
        SignUp: {
            screen: SignUpScreen,
            navigationOptions: {
                title: 'Signup',
                headerTransparent: true

            },
        },
        Drawer: {
            screen: DrawerNavigator,
            navigationOptions: {
                header: null,

            },
        },

    },
    {
        transitionConfig: () => fromLeft(600),

    });

const AppContainer = createAppContainer(stackNavigator);


export default AppContainer;
