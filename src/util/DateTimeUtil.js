import moment from 'moment';

export const getDateTimeFromTimeInMiliSeconds = (timeInMiliSeconds) => {
  const time = moment(timeInMiliSeconds);
  return time.format('DD-MM-YYYY HH:mm:ss');
};
