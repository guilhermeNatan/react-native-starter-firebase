/**
 * Como o aplicativo irá rodar em plataformas(Android e IOs) e dispositivos diferentes,
 * este arquivo tem por finalidade centralizar qualquer tratamento e/ou obter informações do
 * SO bem como características do dispositivo em que está rodando
 */

import { Platform, Dimensions, Keyboard } from 'react-native';
import { Constants } from 'expo';

export default class DeviceFeatures {
  static isAndroid() {
    return Platform.OS === 'android';
  }

  static isIOs() {
    return Platform.OS === 'ios';
  }

  static widthScreen(percent = 100) {
    return (percent * Dimensions.get('window').width) / 100;
  }

  static heigthScreen(percent = 100) {
    return (percent * Dimensions.get('window').height) / 100;
  }

  // A human-readable name for the device type.
  static deviceName() {
    return Constants.deviceName;
  }

  // An identifier that is unique to this particular device and installation of the Expo client.
  static deviceId() {
    return Constants.deviceId;
  }

  // The default status bar height for the device. Does not factor in changes when location tracking is in use or a phone call is active.
  static statusBarHeight() {
    return Constants.statusBarHeight;
  }
}
