export const palette = {
  primaryColor: '#0d47a1',
  primaryLightColor: '#5472d3',
  primaryDarkColor: '#002171',
  secondaryColor: '#29b6f6',
  secondaryLightColor: '#73e8ff',
  secondaryDarkColor: '#0086c3',
  primaryTextColor: '#fafafa',
  secondaryTextColor: '#212121',
  transparent: 'rgba(255, 255, 255, 0)',
  iconColor: '#fafafa',
};
