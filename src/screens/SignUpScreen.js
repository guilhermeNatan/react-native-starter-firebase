import React, { Component } from 'react';
import {
    ActivityIndicator,
    Button, Image, KeyboardAvoidingView, ScrollView, StyleSheet, Text, View,
} from 'react-native';
import t from 'tcomb-form-native';
import { connect } from 'react-redux';
import SnackBar from 'react-native-snackbar-component';
import { signup } from '../actions';
import { getMessageByErroCode } from '../util/HandlerErrorFirebase';
import {profile} from "../resourcers";
import DeviceFeatures from "../util/DeviceFeatures";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        paddingLeft: 60,
        paddingRight: 60,

        marginBottom:  DeviceFeatures.heigthScreen(2),
    },
    scroll: {
        marginTop: DeviceFeatures.heigthScreen(10),
    },
    regisForm: {
        alignSelf: 'stretch',
    },
    profile: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 20,
    },
    title: {
        fontSize: 22,
        color: '#000000',
        padding: 10,
        marginBottom: 20,
        textAlign: 'center',
    },
});

const emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const EMAIL_TYPE = t.refinement(t.String, email => emailRegex.test(email));

const TipoCadastro = t.enums({
    Professor: 'Professor',
    Aluno: 'Aluno',
});

const { Form } = t.form;
const User = t.struct({
    tipoCadastro: TipoCadastro,
    email: EMAIL_TYPE,
    nome: t.String,
    senha: t.String,

});

const options = {
    fields: {
        email: {
            label: 'E-mail',
            error: 'Por favor informe um e-mail válido!',
        },
        senha: {
            secureTextEntry: true,
        },
    },
};

class SignUpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            message: '',
            showSnackBar: false,
            formValue: null,
        };
    }


    componentDidMount() {
    }

    handleSubmit = () => {
        const value = this._form.getValue();
        if (value) {
            const { signup, navigation } = this.props;
            this.setState({ isLoading: true, formValue: value });
            signup(value)
                .then(() => navigation.replace('Drawer'))
                .catch((error) => {
                    this.setState({
                        isLoading: false,
                        showSnackBar: true,
                        formValue: value,
                        message: getMessageByErroCode(error.code),
                    });

                    setTimeout(() => {
                        this.setState({
                            showSnackBar: false,
                        });
                    }, 3000);
                });
        }
    }

    renderButton() {
        const { isLoading } = this.state;
        if (isLoading) {
            return <ActivityIndicator />;
        }

        return (
            <Button
                title="Cadastrar"
                onPress={this.handleSubmit}
            />
        );
    }


    render() {
        const { showSnackBar, message, formValue } = this.state;
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView
                    keyboardVerticalOffset={60}
                    behavior="padding"
                    enabled
                >
                    <ScrollView style={styles.scroll}>
                        <View style={styles.regisForm}>

                            <View style={styles.profile}>
                                <Image
                                    source={profile}
                                    style={styles.addButton}
                                />
                            </View>
                            <Text style={styles.title}> Cadastre-se </Text>
                            <Form
                                ref={c => this._form = c}
                                type={User}
                                options={options}
                                value={formValue}
                            />
                            {
                                this.renderButton()
                            }
                        </View>

                    </ScrollView>

                </KeyboardAvoidingView>

                <SnackBar
                    visible={showSnackBar}
                    textMessage={message}
                    backgroundColor="rgb(16, 48, 108)"
                />
            </View>
        );
    }
}



export default connect(null, { signup })(SignUpScreen);
