import React from 'react';
import {
  View, Button, TextInput, Text, StyleSheet, ActivityIndicator, ImageBackground,
  KeyboardAvoidingView, ScrollView, Dimensions, Image, TouchableOpacity,
} from 'react-native';


import { connect } from 'react-redux';
import firebase from 'firebase';
import SnackBar from 'react-native-snackbar-component';
import FormRow from '../components/FormRow';
import { tryLogin } from '../actions';
import { getMessageByErroCode } from '../util/HandlerErrorFirebase';
import { palette } from '../layout/Colors';
import {bluebg, logo2} from "../resourcers";



const styles = StyleSheet.create({
  container: {

  },
  input: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,

  },
  formRow: {
    backgroundColor: '#FFFFFF',
  },
  form: {
    justifyContent: 'flex-end',
    marginBottom: 10,
    padding: 10,

  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    resizeMode: 'cover',
    backgroundColor: 'rgba(11, 61, 112, 0.4)',

  },
  logo: {
    width: '50%',
    height: '50%',
    marginTop: '5%',
    resizeMode: 'contain',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoText: {
    marginTop: 270,
    fontSize: 30,
    color: palette.primaryTextColor,
    fontWeight: 'bold',
    textShadowColor: 'rgba(255, 255, 255, 0.3)',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 10,
  },
  meCadastrar: {
    textAlign: 'center',
    color: '#ffffff',
    fontWeight: 'bold',
  },
});


class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mail: '',
      password: '',
      isLoading: false,
      showSnackBar: false,
    };
  }

  componentDidMount() {
    const config = {
      apiKey: "AIzaSyB9B1cU-yZhpVMxM7bawgOJ8j3uzCZx5S0",
      authDomain: "react-starter-bf022.firebaseapp.com",
      databaseURL: "https://react-starter-bf022.firebaseio.com",
      projectId: "react-starter-bf022",
      storageBucket: "react-starter-bf022.appspot.com",
      messagingSenderId: "994401233992"
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
  }

onChangeHandler = (field, value) => {
  this.setState({ [field]: value });
}


login = () => {
  const { mail: email, password } = this.state;
  this.setState({ isLoading: true, message: '' });
  const { tryLogin: tryLoginAction, navigation } = this.props;
  tryLoginAction({ email, password })
    .then((user) => {
      if (user) {
        return navigation.replace('Drawer');
      }
      return this.setState({
        isLoading: false,
        message: '',
      });
    })
    .catch((error) => {
      this.setState({
        isLoading: false,
        message: getMessageByErroCode(error.code),
        showSnackBar: true,
      });
      setTimeout(() => {
        this.setState({
          showSnackBar: false,
        });
      }, 3000);
    });
}


renderMenssage = () => {
  const { message, showSnackBar } = this.state;
  if (!message) return null;
  return (
    <SnackBar
      visible={showSnackBar}
      textMessage={message}
      backgroundColor="rgb(16, 48, 108)"
    />


  );
}

renderButton() {
  const { isLoading } = this.state;
  if (isLoading) {
    return <ActivityIndicator />;
  }


  return (
    <Button
      title="Entrar"
      onPress={() => this.login()}
    />
  );
}

render() {
  const { mail, password } = this.state;
  const { navigation } = this.props;
  return (
    <KeyboardAvoidingView keyboardVerticalOffset={3} behavior="position" enabled>
      <ScrollView style={styles.container}>
        <ImageBackground
          source={bluebg}
          style={styles.backgroundImage}
          opacity={0.5}
        >

          <View style={styles.logoContainer}>
            <Image
              source={logo2}
              style={styles.logo}

            />

          </View>

          <View style={styles.form}>

            <FormRow style={styles.formRow}>
              <TextInput
                style={styles.input}
                placeholder="user@email.com"
                value={mail}
                onChangeText={value => this.onChangeHandler('mail', value)}
              />
            </FormRow>
            <FormRow style={styles.formRow}>
              <TextInput
                style={styles.input}
                placeholder="*****"
                secureTextEntry
                value={password}
                onChangeText={value => this.onChangeHandler('password', value)}
              />
            </FormRow>
            <FormRow formRowButton>
              {
                this.renderButton()
            }
            </FormRow>


            <Text
              style={styles.meCadastrar}
              onPress={() => navigation.navigate('SignUp')}
            >
              { 'Desejo me cadastrar '}
            </Text>

          </View>
          {
            this.renderMenssage()
          }
        </ImageBackground>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
}

export default connect(null, { tryLogin })(LoginScreen);
