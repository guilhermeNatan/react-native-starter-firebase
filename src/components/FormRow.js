import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: 'white',
    marginTop: 5,
    marginBottom: 5,
    elevation: 1,
    borderRadius: 10,
  },
  containerForButton: {
    padding: 10,
    marginTop: 5,
    marginBottom: 5,
  },
  first: {
    marginTop: 10,
  },
  last: {
    marginBottom: 10,
  },
});

const FormRow = (props) => {
  const {
    children, first, last, formRowButton, style,
  } = props;
  return (
    <View style={[
      formRowButton ? styles.containerForButton : styles.container,
      first ? styles.first : null,
      last ? styles.last : null,
      style ? { ...style } : null,
    ]}
    >
      {
        children
      }
    </View>

  );
};


export default FormRow;
