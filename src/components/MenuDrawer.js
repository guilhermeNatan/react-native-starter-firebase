import React, {Component} from 'react';
import {
    Dimensions,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import {logo2} from "../resourcers";

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;



class MenuDrawer extends Component {
    navLink(nav, text) {
        return (
            <TouchableOpacity style={{height: 50}} onPress={()=> this.props.navigation.navigate(nav)}>
                <Text style={styles.link}> {text} </Text>
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={styles.container}>
               <ScrollView>
                   <View style={styles.topLinks}>
                       <View style={styles.profile}>
                           <View style={styles.imgView}>
                               <Image style={styles.img} source={logo2}/>

                           </View>
                           <View style={styles.profileText}>
                               <Text style={styles.nomeEmpresa}>Nome Empresa</Text>
                           </View>
                       </View>
                   </View>
                   <View style={styles.bottomLinks}>
                       { this.navLink('Home', 'Home')}
                       { this.navLink('Links', 'Links')}
                       { this.navLink('Settings', 'Settings')}
                   </View>
                   <View style={styles.footer}>
                       <Text style={styles.description}>
                           Menu tutorial
                       </Text>
                       <Text style={styles.version}>
                           v1.0
                       </Text>


                   </View>
               </ScrollView>
            </View>
        );
    }
}


export default MenuDrawer;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgray'
    },
    link: {
        flex:1,
        fontSize: 20,
        padding: 6,
        paddingLeft: 14,
        margin: 5,
        textAlign: 'left'
    },
    topLinks: {
        height: 160,
        backgroundColor: 'black'
    },
    bottomLinks: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 450
    },
    profile: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777'
    },
    imgView:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    img: {
        width: 70,
        height: 70,
        borderRadius: 50
    },
    profileText: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    nomeEmpresa: {
        fontSize: 20,
        paddingBottom: 5,
        color: 'white',
        textAlign: 'left'
    },
    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgray'

    },
    description: {
        flex: 1,
        fontSize: 16 ,
        marginLeft: 20,
    },
    version: {
        flex: 1,
        textAlign: 'right',
        marginRight: 20,
        color: 'gray',
    }

})