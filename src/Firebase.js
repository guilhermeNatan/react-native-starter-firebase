import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyB9B1cU-yZhpVMxM7bawgOJ8j3uzCZx5S0",
    authDomain: "react-starter-bf022.firebaseapp.com",
    databaseURL: "https://react-starter-bf022.firebaseio.com",
    projectId: "react-starter-bf022",
    storageBucket: "react-starter-bf022.appspot.com",
    messagingSenderId: "994401233992"
};
firebase.initializeApp(config);

export default firebase;