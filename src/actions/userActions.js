import firebase from '../Firebase';

export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCES';
export const userLoginSucess = user => ({
  type: USER_LOGIN_SUCCESS,
  user,
});

export const USER_SIGNUP_SUCCESS = 'USER_SIGNUP_SUCCESS';
export const userSignupSuccess = user => ({
  type: USER_SIGNUP_SUCCESS,
  user,
});

export const USER_LOGOUT = 'USER_LOGOUT';
export const userLogout = () => ({
  type: USER_LOGOUT,
});


/**
 * realiza login .
 * @param email
 * @param password
 * @returns {function(*): Promise<firebase.auth.UserCredential | never>}
 */
export const tryLogin = ({ email, password }) => dispacth => firebase
  .auth()
  .signInWithEmailAndPassword(email, password)
  .then((response) => {
    const action = userLoginSucess(response.user);
    dispacth(action);
    return response.user;
  })
  .catch(error => Promise.reject(error));
/**
 * cadastrar um usuário.
 * @returns {function(*=): Promise<firebase.User | never>}
 */
export const signup = (signUpForm) => dispacth => firebase
    .auth()
    .createUserWithEmailAndPassword(signUpForm.email, signUpForm.senha)
    .then((response) => {
        const {user} = response;

        return user.updateProfile({displayName: signUpForm.nome})
            .then(() => user);
    })
    .then((user) => {
        const newRef = firebase
            .database()
            .ref(`/users/${user.uid}/dadosUsuario`)
            .update({nome: signUpForm.nome, tipoCadastro: signUpForm.tipoCadastro})
            .then(() => {
                console.log('feito');
                const action = userSignupSuccess({...user, addressKey: newRef.key});
                dispacth(action);
            });

    })
    .catch(error => Promise.reject(error));
